import org.junit.Assert;

public class MultiplesOf3And5 {

    public static void main(String[] args) {
        tests(233168);
    }

    public static int findMultiples() {
        int x = 0;
        for (int i = 0; i < 1000; i++) {
            if (i%3==0 || i%5==0) {
                System.out.println(i);
                x = x+i;
            }
        }
        System.out.println("Sum of all multiples of 3 and 5 is: " + x);
        return x;
    }

    public static void tests(int sumOfMultiples) {
        Assert.assertEquals(findMultiples(), sumOfMultiples);
    }
}
